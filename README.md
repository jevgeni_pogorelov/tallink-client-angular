# README #

### Client for Conference management system ###

* Create and cancel conferences
* Check conference room availability
* Add remove participants from conference

### Used technologies ###

* [Angular](https://angular.io)
* [Bootstrap](https://getbootstrap.com)
* [Ng-Bootstrap](https://ng-bootstrap.github.io/#/home)
* [Angular l10n](https://robisim74.github.io/angular-l10n/)
* [ngx-toastr](https://github.com/scttcper/ngx-toastr)

### Install ###
```.bash
npm clean install
```

### Run ###
```.bash
npm run serve
```
### Usage ###

```.url
http://localhost:4200/
```
