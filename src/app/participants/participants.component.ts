import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ParticipantService} from '../services/participant/participant.service';
import {ToastrService} from 'ngx-toastr';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {L10nTranslationService} from 'angular-l10n';

@Component({
    selector: 'app-participants',
    templateUrl: './participants.component.html',
    styleUrls: ['./participants.component.css']
})
export class ParticipantsComponent implements OnInit {

    participants: any = [];
    errorMessage: string;

    // paging
    itemsPerPage: number;
    totalItems: any;
    page = 1;
    previousPage: any;

    addParticipantForm: FormGroup;

    addParticipantFormSubmitted = false;

    selectedParticipantId = null;

    // constants
    size = 'size';
    content = 'content';
    totalElements = 'totalElements';

    constructor(private participantService: ParticipantService,
                private toastr: ToastrService,
                private modalService: NgbModal,
                private formBuilder: FormBuilder,
                private translation: L10nTranslationService) {
    }

    async ngOnInit() {
        await this.loadData();
    }

    toggleAddParticipantModal(content) {
        this.initAddParticipantForm();
        this.modalService.open(content, {centered: true, backdrop: false, keyboard: false});
    }

    initAddParticipantForm() {
        this.addParticipantForm = this.formBuilder.group({
            fullName: ['', Validators.required],
            birthDate: [null, Validators.required],
        });
    }

    onRowSelected(participantId) {
        this.selectedParticipantId = participantId;
    }

    addParticipant() {
        this.addParticipantFormSubmitted = true;
        if (this.addParticipantForm.invalid) {
            return;
        }
        this.participantService.saveParticipant(this.addParticipantForm.value)
            .then(() => {
                this.addParticipantForm.reset();
                this.addParticipantFormSubmitted = false;
                this.modalService.dismissAll();
                this.loadData();
                this.toastr.success(this.translation.translate('msg.participant_added'));
            })
            .catch((error) => {
                this.errorMessage = 'Error: ' + error.message;
            });
    }

    get participantForm() {
        return this.addParticipantForm.controls;
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadData();
        }
    }

    loadData() {
        this.participantService.listParticipants({
            page: this.page - 1,
            size: this.itemsPerPage,
        })
            .subscribe(
                data => {
                    this.participants = data[this.content];
                    this.totalItems = data[this.totalElements];
                    this.itemsPerPage = data[this.size];
                },
                error => {
                    this.errorMessage = 'Error: ' + error.message;
                }
            );
    }
}
