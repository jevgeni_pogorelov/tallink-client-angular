import {Component, OnInit} from '@angular/core';
import {ConferenceService} from '../services/conference/conference.service';
import {RoomService} from '../services/room/room.service';
import {ToastrService} from 'ngx-toastr';
import {L10nTranslationService} from 'angular-l10n';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-conferences',
    templateUrl: './conferences.component.html',
    styleUrls: ['./conferences.component.css'],
})
export class ConferencesComponent implements OnInit {

    conferences: any = [];
    availableRooms: any = [];
    errorMessage: string;

    // paging
    itemsPerPage: number;
    totalItems: any;
    page = 1;
    previousPage: any;

    addConferenceForm: FormGroup;
    addRoomForm: FormGroup;

    addConferenceFormSubmitted = false;
    addRoomFormSubmitted = false;

    // constants
    size = 'size';
    content = 'content';
    totalElements = 'totalElements';

    constructor(private conferenceService: ConferenceService,
                private roomService: RoomService,
                private toastr: ToastrService,
                private modalService: NgbModal,
                private formBuilder: FormBuilder,
                private translation: L10nTranslationService) {
    }

    async ngOnInit() {
        await this.loadData();
    }

    toggleAddConferenceModal(content) {
        this.initAddConferenceForm();
        this.openModal(content);
    }

    async toggleAddRoomModal(content, conferenceId) {
        this.initAddRoomForm(conferenceId);
        await this.findAvailableRoomByConferenceId(conferenceId);
        this.openModal(content);
    }

    openModal(content) {
        this.modalService.open(content, {centered: true, backdrop: false, keyboard: false});
    }

    initAddConferenceForm() {
        this.addConferenceForm = this.formBuilder.group({
            name: ['', Validators.required],
            date: [null, Validators.required],
            time: [null, Validators.required]
        });
    }

    initAddRoomForm(conferenceId) {
        this.addRoomForm = this.formBuilder.group({
            roomId: ['', Validators.required],
            conferenceId: [conferenceId, Validators.required],
        });
    }

    addConference() {
        this.addConferenceFormSubmitted = true;
        if (this.addConferenceForm.invalid) {
            return;
        }

        this.conferenceService.saveConference(this.addConferenceForm.value)
            .then(() => {
                this.addConferenceForm.reset();
                this.addConferenceFormSubmitted = false;
                this.modalService.dismissAll();
                this.loadData();
                this.toastr.success(this.translation.translate('msg.conference_added'));
            })
            .catch((error) => {
                this.errorMessage = 'Error: ' + error.message;
            });
    }

    addRoom() {
        this.addRoomFormSubmitted = true;
        if (this.addRoomForm.invalid) {
            return;
        }

        this.conferenceService.addRoomToConference(this.addRoomForm.value)
            .then(() => {
                this.addRoomForm.reset();
                this.addRoomFormSubmitted = false;
                this.modalService.dismissAll();
                this.loadData();
                this.toastr.success(this.translation.translate('msg.room_added'));
            })
            .catch((error) => {
                this.errorMessage = 'Error: ' + error.message;
            });
    }

    cancelConference(conferenceId) {
        this.conferenceService.cancelConference(conferenceId)
            .then(() => {
                this.loadData();
                this.toastr.success(this.translation.translate('msg.conference_canceled'));
            })
            .catch((error) => {
                this.errorMessage = 'Error: ' + error.message;
            });
    }

    get conferenceForm() {
        return this.addConferenceForm.controls;
    }

    get roomForm() {
        return this.addRoomForm.controls;
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadData();
        }
    }

    loadData() {
        this.conferenceService.listConferences({
            page: this.page - 1,
            size: this.itemsPerPage,
        })
            .subscribe(
                data => {
                    this.conferences = data[this.content];
                    this.totalItems = data[this.totalElements];
                    this.itemsPerPage = data[this.size];
                },
                error => {
                    this.errorMessage = 'Error: ' + error.message;
                }
            );

    }

    async findAvailableRoomByConferenceId(conferenceId) {
        try {
            this.availableRooms = await this.roomService.findAvailableRoomByConferenceId(conferenceId);
        } catch (error) {
            this.errorMessage = 'Error: ' + error.message;
        }
    }
}
