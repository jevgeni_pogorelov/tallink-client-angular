import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ConferencesComponent} from './conferences.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ToastrModule} from 'ngx-toastr';
import {l10nConfig} from '../l10n-config';
import {L10nTranslationModule} from 'angular-l10n';

describe('ConferencesComponent', () => {
    let component: ConferencesComponent;
    let fixture: ComponentFixture<ConferencesComponent>;

    beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [ConferencesComponent],
                imports: [HttpClientTestingModule,
                    ReactiveFormsModule,
                    ToastrModule.forRoot(),
                    L10nTranslationModule.forRoot(l10nConfig)]
            })
                .compileComponents();
        })
    )
    ;

    beforeEach(() => {
        fixture = TestBed.createComponent(ConferencesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
})
;
