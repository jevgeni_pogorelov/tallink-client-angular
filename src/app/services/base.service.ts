import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export abstract class BaseService {

  apiContext = environment.apiContext;

  constructor(protected http: HttpClient) { }

  protected getApiContext(): string {
    return this.apiContext;
  }
}
