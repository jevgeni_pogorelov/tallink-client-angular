import { TestBed } from '@angular/core/testing';

import { ParticipantConferenceService } from './participant-conference.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('ParticipantConferenceService', () => {
  let service: ParticipantConferenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(ParticipantConferenceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
