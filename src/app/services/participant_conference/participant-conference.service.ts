import {Injectable} from '@angular/core';
import {BaseService} from '../base.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ParticipantConferenceService extends BaseService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    addParticipantToConference(params: any) {
        return this.http.post(this.getApiContext() + '/participantConference/addToConference', params).toPromise();
    }

    removeParticipantFromConference(params: any) {
        return this.http.delete(this.getApiContext() + '/participantConference/removeFromConference', {params}).toPromise();
    }
}
