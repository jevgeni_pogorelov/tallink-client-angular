import {Injectable} from '@angular/core';
import {BaseService} from '../base.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class RoomService extends BaseService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    listRooms(params: any) {
        return this.http.get(this.getApiContext() + '/room', {params});
    }

    saveRoom(room: any) {
        return this.http.post(this.getApiContext() + '/room', room).toPromise();
    }

    findAvailableRoomByConferenceId(conferenceId: string) {
        return this.http.get(this.getApiContext() + '/room/' + conferenceId).toPromise();
    }
}
