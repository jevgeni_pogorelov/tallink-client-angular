import { Injectable } from '@angular/core';
import {BaseService} from '../base.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ParticipantService extends BaseService {

  constructor(protected http: HttpClient) {
    super(http);
  }

  listParticipants(params: any) {
    return this.http.get(this.getApiContext() + '/participant', {params});
  }

  saveParticipant(participant: any) {
    return this.http.post(this.getApiContext() + '/participant', participant).toPromise();
  }

  getAllParticipantConferences(participantId: string) {
    return this.http.get(this.getApiContext() + '/participant/participantConferences/' + participantId);
  }
}
