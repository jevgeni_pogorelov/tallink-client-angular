import {Injectable} from '@angular/core';
import {BaseService} from '../base.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ConferenceService extends BaseService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    listConferences(params: any) {
        return this.http.get(this.getApiContext() + '/conference', {params});
    }

    saveConference(conference: any) {
        return this.http.post(this.getApiContext() + '/conference', conference).toPromise();
    }

    cancelConference(conferenceId) {
        return this.http.post(this.getApiContext() + '/conference/cancel/' + conferenceId, null).toPromise();
    }

    addRoomToConference(addRoomModel: any) {
        return this.http.post(this.getApiContext() + '/conference/addRoom', addRoomModel).toPromise();
    }

    findAvailableConferencesToParticipate(params: any) {
        return this.http.get(this.getApiContext() + '/conference/availableToParticipate', {params});
    }

}
