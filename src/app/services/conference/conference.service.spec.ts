import { TestBed } from '@angular/core/testing';

import { ConferenceService } from './conference.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('ConferenceService', () => {
  let service: ConferenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(ConferenceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
