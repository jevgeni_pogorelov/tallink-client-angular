import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {ParticipantService} from '../services/participant/participant.service';
import {ParticipantConferenceService} from '../services/participant_conference/participant-conference.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {L10nTranslationService} from 'angular-l10n';
import {ConferenceService} from '../services/conference/conference.service';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-participant-conferences',
    templateUrl: './participant-conferences.component.html',
    styleUrls: ['./participant-conferences.component.css']
})
export class ParticipantConferencesComponent implements OnInit, OnChanges {

    @Input() participantId;

    participantDetails: any = {};
    errorMessage: string;

    // paging
    itemsPerPage: number;
    totalItems: any;
    page = 1;
    previousPage: any;

    availableConferences: any = [];
    // constants
    size = 'size';
    content = 'content';
    totalElements = 'totalElements';


    constructor(private participantService: ParticipantService,
                private participantConferenceService: ParticipantConferenceService,
                private conferenceService: ConferenceService,
                private modalService: NgbModal,
                private translation: L10nTranslationService,
                private toastr: ToastrService) {
    }

    ngOnChanges() {
        this.loadData();
    }

    async ngOnInit() {
        await this.loadData();
    }

    async toggleAddToConferenceModal(content) {
        await this.loadAvailableConferences();
        this.modalService.open(content, {size: 'lg', centered: true, backdrop: false, keyboard: false});
    }

    selectConference(conferenceId) {
        if (!conferenceId || !this.participantId) {
            this.errorMessage = this.translation.translate('msg.missing_participant_or_conference_id');
            this.modalService.dismissAll();
            return;
        }
        this.participantConferenceService.addParticipantToConference({
            participantId: this.participantId,
            conferenceId,
        })
            .then(() => {
                this.modalService.dismissAll();
                this.loadData();
                this.toastr.success(this.translation.translate('msg.added_to_conference'));
            }).catch((error => {
            this.toastr.error(error.response.data.message);
        }));
    }

    removeFromConference(conferenceId) {
        if (!conferenceId || !this.participantId) {
            this.errorMessage = this.translation.translate('msg.missing_participant_or_conference_id');
            return;
        }
        this.participantConferenceService.removeParticipantFromConference({
            participantId: this.participantId,
            conferenceId,
        })
            .then(() => {
                this.modalService.dismissAll();
                this.loadData();
                this.toastr.success(this.translation.translate('msg.removed_from_conference'));
            }).catch((error) => {
            this.toastr.error(error.response.data.message);
        });
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAvailableConferences();
        }
    }

    loadData() {
        this.participantService.getAllParticipantConferences(this.participantId)
            .subscribe(
                data => {
                    this.participantDetails = data;
                },
                error => {
                    this.errorMessage = 'Error: ' + error.message;
                }
            );
    }

    loadAvailableConferences() {
        this.conferenceService.findAvailableConferencesToParticipate({
            page: this.page - 1,
            size: this.itemsPerPage,
            participantId: this.participantId,
        })
            .subscribe(
                data => {
                    this.availableConferences = data[this.content];
                    this.totalItems = data[this.totalElements];
                    this.itemsPerPage = data[this.size];
                },
                error => {
                    this.errorMessage = 'Error: ' + error.message;
                }
            );
    }

}
