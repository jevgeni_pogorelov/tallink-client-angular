import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ParticipantConferencesComponent} from './participant-conferences.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ToastrModule} from 'ngx-toastr';
import {L10nTranslationModule} from 'angular-l10n';
import {l10nConfig} from '../l10n-config';

describe('ParticipantConferencesComponent', () => {
    let component: ParticipantConferencesComponent;
    let fixture: ComponentFixture<ParticipantConferencesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ParticipantConferencesComponent],
            imports: [HttpClientTestingModule,
                ToastrModule.forRoot(),
                L10nTranslationModule.forRoot(l10nConfig)]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ParticipantConferencesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
