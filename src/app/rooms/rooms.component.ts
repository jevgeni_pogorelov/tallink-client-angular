import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RoomService} from '../services/room/room.service';
import {ToastrService} from 'ngx-toastr';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {L10nTranslationService} from 'angular-l10n';

@Component({
    selector: 'app-rooms',
    templateUrl: './rooms.component.html',
    styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {

    rooms: any = [];
    errorMessage: string;

    // paging
    itemsPerPage: number;
    totalItems: any;
    page = 1;
    previousPage: any;

    addRoomForm: FormGroup;

    addRoomFormSubmitted = false;

    // constants
    size = 'size';
    content = 'content';
    totalElements = 'totalElements';

    constructor(private roomService: RoomService,
                private toastr: ToastrService,
                private modalService: NgbModal,
                private formBuilder: FormBuilder,
                private translation: L10nTranslationService) {
    }

    async ngOnInit() {
        await this.loadData();
    }

    toggleAddRoomModal(content) {
        this.initAddRoomForm();
        this.modalService.open(content, {centered: true, backdrop: false, keyboard: false});
    }

    initAddRoomForm() {
        this.addRoomForm = this.formBuilder.group({
            name: ['', Validators.required],
            location: ['', Validators.required],
            maxSeats: [null, Validators.required],
        });
    }

    addRoom() {
        this.addRoomFormSubmitted = true;
        if (this.addRoomForm.invalid) {
            return;
        }
        this.roomService.saveRoom(this.addRoomForm.value)
            .then(() => {
                this.addRoomForm.reset();
                this.addRoomFormSubmitted = false;
                this.modalService.dismissAll();
                this.loadData();
                this.toastr.success(this.translation.translate('msg.room_added'));
            })
            .catch((error) => {
                this.errorMessage = 'Error: ' + error.message;
            });
    }

    get roomForm() {
        return this.addRoomForm.controls;
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadData();
        }
    }

    loadData() {
        this.roomService.listRooms({
            page: this.page - 1,
            size: this.itemsPerPage,
        })
            .subscribe(
                data => {
                    this.rooms = data[this.content];
                    this.totalItems = data[this.totalElements];
                    this.itemsPerPage = data[this.size];
                },
                error => {
                    this.errorMessage = 'Error: ' + error.message;
                }
            );
    }

}
