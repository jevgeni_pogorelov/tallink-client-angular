import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { L10nConfig, L10nTranslationModule, L10nLoader } from 'angular-l10n';

import { NavbarComponent } from './navbar.component';
import {l10nConfig} from '../l10n-config';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      declarations: [ NavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
