import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgbDateAdapter, NgbDateNativeAdapter, NgbModule, NgbTimeAdapter} from '@ng-bootstrap/ng-bootstrap';
import {L10nIntlModule, L10nLoader, L10nRoutingModule, L10nTranslationModule, L10nValidationModule} from 'angular-l10n';
import {AppStorage, HttpTranslationLoader, initL10n, l10nConfig, LocaleValidation} from './l10n-config';
import { NavbarComponent } from './navbar/navbar.component';
import { ConferencesComponent } from './conferences/conferences.component';
import { ParticipantsComponent } from './participants/participants.component';
import { RoomsComponent } from './rooms/rooms.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NgbTimeStringAdapter} from './utils/adapters/timepicker-adapter';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import { ParticipantConferencesComponent } from './participant-conferences/participant-conferences.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ConferencesComponent,
    ParticipantsComponent,
    RoomsComponent,
    ParticipantConferencesComponent

  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        NgbModule,
        L10nTranslationModule.forRoot(
            l10nConfig,
            {
                storage: AppStorage,
                translationLoader: HttpTranslationLoader
            }
        ),
        L10nIntlModule,
        L10nValidationModule.forRoot({validation: LocaleValidation}),
        L10nRoutingModule.forRoot(),
        ToastrModule.forRoot()
    ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initL10n,
      deps: [L10nLoader],
      multi: true
    },
      {provide: NgbTimeAdapter, useClass: NgbTimeStringAdapter},
      {provide: NgbDateAdapter, useClass: NgbDateNativeAdapter}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
