import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ConferencesComponent} from './conferences/conferences.component';
import {ParticipantsComponent} from './participants/participants.component';
import {RoomsComponent} from './rooms/rooms.component';


const routes: Routes = [
    {path: '', redirectTo: '/conferences', pathMatch: 'full'},
    {path: 'conferences', component: ConferencesComponent},
    {path: 'participants', component: ParticipantsComponent},
    {path: 'rooms', component: RoomsComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
